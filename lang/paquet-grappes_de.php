<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grappes?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'grappes_description' => 'Neue Objekte (Trauben) künnen aus SPIP-Objekten zusammengestellt werden,
		wenn diese ein Interface haben.',
	'grappes_nom' => 'Trauben',
	'grappes_slogan' => 'SPIP-Objekte in einer gemeinsamen Umgebung gruppieren'
);
