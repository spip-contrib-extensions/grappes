<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grappes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'grappes_description' => 'Allows to group together elements of SPIP
        such as authors, sections, articles (when a dedicated interface exists)...
        in a same universe (just like a bunch of grapes)',
	'grappes_nom' => 'Bunches of grapes',
	'grappes_slogan' => 'Regroup SPIP objets SPIP in the same universe'
);
