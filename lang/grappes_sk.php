<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/grappes?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_inconnue' => 'Akcia "@action@" neznáma',
	'autoriser_associer_non' => 'Na vykonanie tejto akcie nemáte dostatočné oprávnenia.',

	// B
	'bouton_ajouter' => 'Pridať',
	'bouton_checkbox_qui_administrateurs' => 'Správcovia',
	'bouton_checkbox_qui_id_admin' => 'Autor zväzku (Vy?)',
	'bouton_checkbox_qui_redacteurs' => 'Redaktori',
	'bouton_chercher' => 'Vyľadať',

	// D
	'delier' => 'Rozpojiť',
	'delier_tout' => 'Všetko rozpojiť',

	// I
	'icone_creation_grappe' => 'Vytvoriť nový zväzok',
	'icone_dupliquer_grappe' => 'Duplikovať tento zväzok',
	'icone_modifier_grappe' => 'Upraviť tento zväzok',
	'icone_supprimer_grappe' => 'Odstrániť tento zväzok',
	'icone_voir_toutes_grappes' => 'Zobraziť všetky zväzky',
	'info_1_grappe' => '1 zväzok',
	'info_aucune_grappe' => 'Žiaden zväzok',
	'info_changer_nom_grappe' => 'Zmeniť názov zväzku', # MODIF
	'info_creation_grappes' => 'Vytvoriť alebo upraviť zväzky objektov',
	'info_grappes_association' => 'Aké objekty možno spojiť do zväzku?', # MODIF
	'info_grappes_miennes' => 'Moje zväzky',
	'info_grappes_toutes' => 'Všetky zväzky',
	'info_nb_grappes' => '@nb@ zväzkov',
	'info_nom_grappe' => 'Názov zväzku', # MODIF
	'info_qui_peut_lier' => 'Čo môže spojiť objekty do zväzku?',
	'info_rechercher' => 'Vyhľadať',
	'item_groupes_association_articles' => 'Články',
	'item_groupes_association_auteurs' => 'Články',
	'item_groupes_association_breves' => 'Novinky',
	'item_groupes_association_documents' => 'Dokumenty',
	'item_groupes_association_grappes' => 'Zväzky',
	'item_groupes_association_groupes_mots' => 'Skupiny kľúčových slov',
	'item_groupes_association_mots' => 'Slová',
	'item_groupes_association_rubriques' => 'Rubriky',
	'item_groupes_association_syndic' => 'Stránky',

	// L
	'label_acces' => 'Prístup',
	'label_liaisons' => 'Objekt',
	'label_type' => 'Typ',
	'lier' => 'Odkaz',

	// P
	'pas_de_identifiant' => 'Neznámy názov',

	// T
	'texte_descriptif' => 'Opis',
	'titre_grappe' => 'Zväzok',
	'titre_grappes' => 'Zväzky',
	'titre_logo_grappe' => 'Logo zväzku',
	'titre_nouvelle_grappe' => 'Nový zväzok',
	'titre_page_grappes' => 'Zväzky',

	// V
	'voir' => 'Zobraziť'
);
