<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grappes?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'grappes_description' => 'Permite agrupar elementos de SPIP
        como autores/as, secciones, artículos (cuando hay una interfaz que lo permite)...
        en un mismo universo (un racimo)',
	'grappes_nom' => 'Racimos',
	'grappes_slogan' => 'Reagrupar los objetos SPIP en un mismo universo'
);
