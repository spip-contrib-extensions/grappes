<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grappes?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'grappes_description' => 'Ti consente di raggruppare elementi di SPIP come autori, argomenti, articoli (quando esiste l’interfaccia per)...
nello stesso universo (un cluster)',
	'grappes_nom' => 'Cluster',
	'grappes_slogan' => 'Raggruppamento di oggetti SPIP nello stesso universo'
);
