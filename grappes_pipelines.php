<?php

/**
 * Plugin Grappes
 * Licence GPL (c) Matthieu Marcillaud
 *
 * Fichier de pipelines du plugin
 *
 * @package Grappes\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Indique le chemin du script sortable
 *
 * @param string $flux
 * @return string
 */
function grappes_header_prive($flux) {

	$sortable = timestamp(find_in_path('prive/javascript/Sortable.min.js'));
	$flux .= <<<JAVASCRIPT
	<script type="text/javascript">
		window.spipConfig ??= {};
		spipConfig.sortable ??= {};
		spipConfig.sortable.script ??= '$sortable';
	</script>
	JAVASCRIPT;

	$flux .= "<script type='text/javascript' src='" . find_in_path('javascript/Grappes.js') . "'></script>\n";

	return $flux;
}


/**
 * Insertion dans le pipeline afficher_contenu_objet (SPIP)
 *
 * Ajouter le bloc des grappes aux pages d'objets pouvant être liés à une grappe
 *
 * @param array $flux
 * 	Le contexte du pipeline
 * @return array $flux
 * 	Le contexte du pipeline modifié
 */
function grappes_afficher_contenu_objet($flux) {
	if (
		($objet = $flux['args']['type'])
		&& ($id_objet = (int) $flux['args']['id_objet'])
		&& sql_countsel('spip_grappes', "liaisons REGEXP '(^|,)" . table_objet($objet) . "($|,)'")
	) {
		$texte = recuperer_fond(
			'prive/squelettes/inclure/grappes_lister_objets',
			[
				'objet' => 'grappes',
				'source' => $objet,
				'id_source' => $id_objet
			]
		);
		$flux['data'] .= $texte;
	}

	return $flux;
}

/**
 * Insertion dans le pipeline grappes_objets_lies (Plugin Grappes)
 *
 * Définit le tableau des objets pouvant être liés aux grappes, la clé est le type d'objet (au pluriel),
 * la valeur, le label affiché dans le formulaire d'édition de grappe
 *
 * @param array $array
 * 	Le tableau du pipeline
 * @return array $array
 * 	Le tableau complété
 */
function grappes_grappes_objets_lies($array) {
	$array = is_array($array) ? $array : [];
	$array['articles'] = _T('grappes:item_groupes_association_articles');
	$array['auteurs'] = _T('grappes:item_groupes_association_auteurs');
	$array['breves'] = _T('grappes:item_groupes_association_breves');
	$array['groupes_mots'] = _T('grappes:item_groupes_association_groupes_mots');
	$array['mots'] = _T('grappes:item_groupes_association_mots');
	$array['rubriques'] = _T('grappes:item_groupes_association_rubriques');
	$array['documents'] = _T('grappes:item_groupes_association_documents');
	$array['syndic'] = _T('grappes:item_groupes_association_syndic');

	return $array;
}
