class Grappes {

	/**
	 * Trie une liste d’éléments dans une grappe
	 * @param {string} selector
	 * @param {Object} args
	 * @returns
	 */
	static sortable(selector, args = {}) {
		const table = document.querySelector(selector);
		if (!table) {
			return;
		}
		Grappes.loadScript(spipConfig.sortable.script, () => {
			// détruire / recréer le sortable à chaque appel ajax
			if (Sortable.get(table)) {
				Sortable.get(table).destroy();
			}
			// pas de tri possible s'il n'y a qu'un seul élément.
			if (table.querySelectorAll('tr').length < 2) {
				return;
			}
			table.querySelectorAll('tr').forEach(tr => tr.dataset.id ??= tr.getAttribute('id'));
			new Sortable(table, {
				direction: 'vertical',
				ghostClass: 'deplacer-lien-grappe',
				onUpdate: function(event) {
					const ordre = this.toArray().join(',');
					jQuery.ajax({
						type: "POST",
						url: "?action=trier_objets",
						data: {...args, ...{sort: ordre}}
					}).done(() => {
						jQuery(table).ajaxReload(); // Une fois trié on recharge la liste concernée
					});
				}
			});
		});
	}

	static loadScript = (url, callback) => {
		let script;
		const scripts = Array.from(document.querySelectorAll('script'));
		const existingScript = scripts.find((script) => script.src === url);
		if (existingScript) {
			script = existingScript;
		} else {
			script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = url;
			document.getElementsByTagName('head')[0].appendChild(script);
		}

		if (script.readyState) {
			script.onreadystatechange = () => {
				if (script.readyState === 'loaded' || script.readyState === 'complete') {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else {
			script.onload = () => callback();
		}
	};

	static onReady(fn) {
		if (document.readyState !== 'loading') {
			fn();
		} else {
			document.addEventListener('DOMContentLoaded', fn);
		}
	}
}
